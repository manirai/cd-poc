package net.manirai.cdpoc.testservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("another-test-service")
public interface TestFeignClient {

    @GetMapping
    String sayHelloWorld();

    @GetMapping("/hello")
    String sayHelloWorld1();
}
