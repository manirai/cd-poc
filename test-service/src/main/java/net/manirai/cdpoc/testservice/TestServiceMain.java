package net.manirai.cdpoc.testservice;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
@EnableFeignClients
@EnableCircuitBreaker
public class TestServiceMain {

    @Value("${test.string}")
    private String testString;

    @Autowired
    private TestFeignClient testFeignClient;

    public static void main(String[] args) {
        SpringApplication.run(TestServiceMain.class, args);
    }

    @GetMapping
    public String sayHelloWorld() {
        return "Hello World 1";
    }

    @GetMapping("/ic")
    public String testIc() {
        return this.testFeignClient.sayHelloWorld();
    }

    @GetMapping("/config")
    public String testConfig() {
        return this.testString;
    }

    @HystrixCommand(fallbackMethod = "defaultGreeting")
    @GetMapping("/htx")
    public String hystrixTest() {
        return this.testFeignClient.sayHelloWorld1();
    }

    private String defaultGreeting() {
        return "Hello User!";
    }
}
