package net.manirai.cdpoc.apigatewayservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GatewayServiceMain {

    public static void main(String[] args) {
        SpringApplication.run(GatewayServiceMain.class, args);
    }
}
