package net.manirai.cdpoc.discoveryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DiscoveryServiceMain {

    public static void main(String[] args) {
        SpringApplication.run(DiscoveryServiceMain.class, args);
    }
}
