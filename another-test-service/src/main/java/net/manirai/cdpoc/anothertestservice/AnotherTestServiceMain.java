package net.manirai.cdpoc.anothertestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
public class AnotherTestServiceMain {

    public static void main(String[] args) {
        SpringApplication.run(AnotherTestServiceMain.class);
    }

    @GetMapping
    public String sayHelloWorld() {
        return "Hello World from another test service.";
    }
}
